<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'name',
        'surname',
        'patronymic',
        'avatar'
    ];

    public function getAvatarAttribute($val)
    {
        return $val ? asset('storage/avatars/'.$val) : asset('img/default-avatar.jpg');
    }
}

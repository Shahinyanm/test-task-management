<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const  STATUSES = [
        1 => 'TO DO',
        2 => 'DOING',
        3 => 'DONE'
    ];

    protected $fillable = [
        'title',
        'description',
        'status_id'
    ];

    public function getStatusAttribute()
    {
        return self::STATUSES[$this->status_id];
    }

    public function employees()
    {
        return $this->belongsToMany(Staff::class);
    }
}

<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\StoreStaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;
use App\Http\Resources\Staff\StaffCollection;
use App\Http\Resources\Staff\StaffResource;
use App\Models\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * @return StaffCollection
     */
    public function index(Request $request)
    {
        $fullName = $request->fullName;
        $task = $request->task;

        $staff = Staff::when(
            $fullName,
            function ($query, $fullName) {
                return $query->where('name', 'like', $fullName.'%')
                    ->orWhere('surname', 'like', $fullName.'%')
                    ->orWhere('patronymic', 'like', $fullName.'%');
            }
        )->when(
            $task,
            function ($query, $task) {
                return $query;
            }
        )->get();

        return new StaffCollection($staff);
    }

    /**
     * @param  StoreStaffRequest  $request
     * @return StaffResource
     */
    public function store(StoreStaffRequest $request)
    {
        $staff = new Staff($request->validated());

        if ($request->has('avatar')) {
            $avatarName = 'avatar'.time().'.'.$request->validated()['avatar']->getClientOriginalExtension();
            $request->validated()['avatar']->storeAs('avatars', $avatarName);
            $staff->avatar = $avatarName;
        }

        if ($staff->save()) {
            return new StaffResource($staff);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param  UpdateStaffRequest  $request
     * @param  Staff              $staff
     * @return StaffResource
     */
    public function update(UpdateStaffRequest $request, Staff $staff)
    {
        $data = $request->validated();

        if ($request->has('avatar')) {
            $avatarName = 'avatar'.time().'.'.$request->validated()['avatar']->getClientOriginalExtension();
            $request->validated()['avatar']->storeAs('avatars', $avatarName);
            $data['avatar'] = $avatarName;
        }

        if ($staff->update($data)) {
            return new StaffResource($staff);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

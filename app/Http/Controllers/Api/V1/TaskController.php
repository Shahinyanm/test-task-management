<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Http\Resources\Task\TaskCollection;
use App\Http\Resources\Task\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * @param  Request  $request
     * @return TaskCollection
     */
    public function index(Request $request)
    {
        $title = $request->title;
        $description = $request->description;
        $status_id = $request->status_id;

        $tasks = Task::when(
            $title,
            function ($query, $title) {
                return $query->where('title', 'like', '%'.$title.'%');
            }
        )->when(
            $description,
            function ($query, $description) {
                return $query->where('description', 'like', '%'.$description.'%');
            }
        )->when(
            $status_id,
            function ($query, $status_id) {
                return $query->where('status_id', $status_id);
            }
        )->with('employees')->get();

        return new TaskCollection($tasks);
    }

    /**
     * @param  StoreTaskRequest  $request
     * @return TaskResource
     */
    public function store(StoreTaskRequest $request)
    {
        $task = new Task($request->validated());

        if ($task->save()) {
            $employees = collect($request->validated()['employees'])->pluck('id');
            $task->employees()->sync($employees);
            return new TaskResource($task->load('employees'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param  UpdateTaskRequest  $request
     * @param  Task               $task
     * @return TaskResource
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $data = $request->validated();

        if ($task->update($data)) {
            $employees = collect($request->validated()['employees'])->pluck('id');
            $task->employees()->sync($employees);
            return new TaskResource($task->load('employees'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

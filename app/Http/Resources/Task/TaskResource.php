<?php

namespace App\Http\Resources\Task;

use App\Http\Resources\Staff\StaffCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'status' => $this->status,
            'status_id' => $this->status_id,
            'employees' => new StaffCollection($this->whenLoaded('employees')),
            'created_at' => $this->created_at->format('m/d/Y H:i:s')
        ];
    }
}

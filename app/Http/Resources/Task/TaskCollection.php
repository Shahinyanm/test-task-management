<?php

namespace App\Http\Resources\Task;

use App\Models\Task;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Task $task) {
            return (new TaskResource($task));
        });

        return parent::toArray($request);
    }
}

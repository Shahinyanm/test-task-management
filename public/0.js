(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/ErrorsMixin */ "./resources/js/mixins/ErrorsMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CreateStaffComponent',
  data: function data() {
    return {
      previewImage: {
        src: ''
      },
      loading: false,
      form: {
        name: '',
        surname: '',
        patronymic: '',
        avatar: null
      }
    };
  },
  mixins: [_mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__["ErrorsMixin"]],
  methods: {
    createStaff: function createStaff() {
      var _this = this;

      this.loading = true;
      this.errors = [];
      var formData = new FormData();
      formData.append('name', this.form.name);
      formData.append('surname', this.form.surname);
      formData.append('patronymic', this.form.patronymic);

      if (this.form.avatar) {
        formData.append('avatar', this.form.avatar);
      }

      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].post('/staff', formData).then(function (r) {
        _this.$buefy.notification.open({
          message: 'Employee created successfully!',
          type: 'is-success'
        });

        _this.$emit('createdEmployee', r.data.data);

        _this.loading = false;
        _this.form = {};
        _this.previewImage = {
          src: ''
        };

        _this.$emit('close');
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this.errors = error.response.data.errors;
        }

        _this.loading = false;
      });
    },
    handleFileInput: function handleFileInput(image) {
      var self = this;
      self.form.avatar = image;
      var reader = new FileReader();

      reader.onload = function (e) {
        self.previewImage = {
          src: e.target.result
        };
      };

      reader.readAsDataURL(image);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/ErrorsMixin */ "./resources/js/mixins/ErrorsMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EditStaffComponent',
  props: ['employee'],
  data: function data() {
    return {
      previewImage: {
        src: this.employee.avatar
      },
      loading: false,
      form: {
        name: this.employee.name,
        surname: this.employee.surname,
        patronymic: this.employee.patronymic,
        avatar: null
      }
    };
  },
  mixins: [_mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__["ErrorsMixin"]],
  methods: {
    editStaff: function editStaff() {
      var _this = this;

      this.loading = true;
      this.errors = [];
      var formData = new FormData();
      formData.append('name', this.form.name);
      formData.append('surname', this.form.surname);
      formData.append('patronymic', this.form.patronymic);

      if (this.form.avatar) {
        formData.append('avatar', this.form.avatar);
      }

      formData.append('_method', 'PUT');
      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].post('/staff/' + this.employee.id, formData).then(function (r) {
        _this.$buefy.notification.open({
          message: 'Employee created successfully!',
          type: 'is-success'
        });

        _this.$emit('updatedEmployee', r.data.data);

        _this.loading = false;
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this.errors = error.response.data.errors;
        }

        _this.loading = false;
      });
    },
    handleFileInput: function handleFileInput(image) {
      var self = this;
      self.form.avatar = image;
      var reader = new FileReader();

      reader.onload = function (e) {
        self.previewImage = {
          src: e.target.result
        };
      };

      reader.readAsDataURL(image);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/StaffPage.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _components_Staff_CreateStaffComponent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Staff/CreateStaffComponent */ "./resources/js/components/Staff/CreateStaffComponent.vue");
/* harmony import */ var _components_Staff_EditStaffComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Staff/EditStaffComponent */ "./resources/js/components/Staff/EditStaffComponent.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'StaffPage',
  components: {
    CreateStaffComponent: _components_Staff_CreateStaffComponent__WEBPACK_IMPORTED_MODULE_1__["default"],
    EditStaffComponent: _components_Staff_EditStaffComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      loading: false,
      isComponentModalActive: false,
      isEditComponentModalActive: false,
      activeEmployeeData: {},
      staff: [],
      searchData: {
        fullName: null,
        task: null
      }
    };
  },
  computed: {
    results: function results() {
      return this.staff;
    }
  },
  watch: {
    searchData: {
      handler: function handler(val) {
        var data = JSON.parse(JSON.stringify(val));
        this.getStaff(data);
      },
      deep: true
    }
  },
  created: function created() {
    this.getStaff();
  },
  methods: {
    getStaff: function getStaff() {
      var _this = this;

      var filter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.loading = true;

      if (filter) {
        filter = Object.keys(filter).filter(function (key) {
          return filter[key] !== 0;
        }).reduce(function (obj, key) {
          obj[key] = filter[key];
          return obj;
        }, {});
      }

      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].get('/staff', {
        params: filter
      }).then(function (r) {
        _this.staff = r.data.data;
        _this.loading = false;
      });
    },
    addEmployeeToList: function addEmployeeToList(employee) {
      this.staff.push(employee);
    },
    updateEmployeeFromList: function updateEmployeeFromList(employee) {
      var foundIndex = this.staff.findIndex(function (x) {
        return x.id === employee.id;
      });
      this.staff[foundIndex] = employee;
    },
    openEditModal: function openEditModal(employee) {
      this.activeEmployeeData = employee;
      this.isEditComponentModalActive = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "img[data-v-139ca69a] {\n  height: 124px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "img[data-v-2c2acac1] {\n  height: 124px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      on: {
        submit: function($event) {
          $event.preventDefault()
          return _vm.createStaff($event)
        }
      }
    },
    [
      _c(
        "div",
        { staticClass: "modal-card", staticStyle: { width: "640px" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "section",
            { staticClass: "modal-card-body" },
            [
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Name",
                    type: { "is-danger": _vm.hasError("name") },
                    message: _vm.getError("name")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Employee Name",
                      required: ""
                    },
                    model: {
                      value: _vm.form.name,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "name", $$v)
                      },
                      expression: "form.name"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Surname",
                    type: { "is-danger": _vm.hasError("surname") },
                    message: _vm.getError("surname")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Employee Surname",
                      required: ""
                    },
                    model: {
                      value: _vm.form.surname,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "surname", $$v)
                      },
                      expression: "form.surname"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Patronymic",
                    type: { "is-danger": _vm.hasError("patronymic") },
                    message: _vm.getError("patronymic")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Employee Patronymic",
                      required: ""
                    },
                    model: {
                      value: _vm.form.patronymic,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "patronymic", $$v)
                      },
                      expression: "form.patronymic"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Avatar",
                    type: { "is-danger": _vm.hasError("avatar") },
                    message: _vm.getError("avatar")
                  }
                },
                [
                  _c(
                    "b-upload",
                    {
                      ref: "upload",
                      attrs: { accept: "image/*", "drag-drop": "" },
                      on: { input: _vm.handleFileInput },
                      model: {
                        value: _vm.form.avatar,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "avatar", $$v)
                        },
                        expression: "form.avatar"
                      }
                    },
                    [
                      _c("section", { staticClass: "section" }, [
                        _c(
                          "div",
                          { staticClass: "content has-text-centered" },
                          [
                            _vm.previewImage.src.length
                              ? _c("p", [
                                  _c("img", {
                                    staticClass: "upload-image",
                                    attrs: { src: _vm.previewImage.src }
                                  })
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v("Drop your avatar here or click to upload")
                            ])
                          ]
                        )
                      ])
                    ]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "footer",
            { staticClass: "modal-card-foot" },
            [
              _c(
                "b-button",
                {
                  attrs: {
                    type: "is-primary",
                    loading: _vm.loading,
                    "native-type": "submit"
                  }
                },
                [_vm._v("Create")]
              )
            ],
            1
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "modal-card-head" }, [
      _c("p", { staticClass: "modal-card-title" }, [_vm._v("Create Employee")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      on: {
        submit: function($event) {
          $event.preventDefault()
          return _vm.editStaff($event)
        }
      }
    },
    [
      _c(
        "div",
        { staticClass: "modal-card", staticStyle: { width: "640px" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "section",
            { staticClass: "modal-card-body" },
            [
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Name",
                    type: { "is-danger": _vm.hasError("name") },
                    message: _vm.getError("name")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Employee Name",
                      required: ""
                    },
                    model: {
                      value: _vm.form.name,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "name", $$v)
                      },
                      expression: "form.name"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Surname",
                    type: { "is-danger": _vm.hasError("surname") },
                    message: _vm.getError("surname")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Employee Surname",
                      required: ""
                    },
                    model: {
                      value: _vm.form.surname,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "surname", $$v)
                      },
                      expression: "form.surname"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Patronymic",
                    type: { "is-danger": _vm.hasError("patronymic") },
                    message: _vm.getError("patronymic")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Employee Patronymic",
                      required: ""
                    },
                    model: {
                      value: _vm.form.patronymic,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "patronymic", $$v)
                      },
                      expression: "form.patronymic"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Avatar",
                    type: { "is-danger": _vm.hasError("avatar") },
                    message: _vm.getError("avatar")
                  }
                },
                [
                  _c(
                    "b-upload",
                    {
                      ref: "upload",
                      attrs: { accept: "image/*", "drag-drop": "" },
                      on: { input: _vm.handleFileInput },
                      model: {
                        value: _vm.form.avatar,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "avatar", $$v)
                        },
                        expression: "form.avatar"
                      }
                    },
                    [
                      _c("section", { staticClass: "section" }, [
                        _c(
                          "div",
                          { staticClass: "content has-text-centered" },
                          [
                            _vm.previewImage.src.length
                              ? _c("p", [
                                  _c("img", {
                                    staticClass: "upload-image",
                                    attrs: { src: _vm.previewImage.src }
                                  })
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v("Drop your avatar here or click to upload")
                            ])
                          ]
                        )
                      ])
                    ]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "footer",
            { staticClass: "modal-card-foot" },
            [
              _c(
                "b-button",
                {
                  attrs: {
                    type: "is-primary",
                    loading: _vm.loading,
                    "native-type": "submit"
                  }
                },
                [_vm._v("Update")]
              )
            ],
            1
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "modal-card-head" }, [
      _c("p", { staticClass: "modal-card-title" }, [_vm._v("Edit Employee")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "button",
        {
          staticClass: "button is-primary is-pulled-right",
          on: {
            click: function($event) {
              _vm.isComponentModalActive = true
            }
          }
        },
        [_vm._v("\n        Create Employee\n    ")]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            active: _vm.isComponentModalActive,
            width: 640,
            "has-modal-card": "",
            "trap-focus": "",
            "aria-role": "dialog",
            "aria-modal": ""
          },
          on: {
            "update:active": function($event) {
              _vm.isComponentModalActive = $event
            }
          }
        },
        [
          _c("create-staff-component", {
            on: { createdEmployee: _vm.addEmployeeToList }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            active: _vm.isEditComponentModalActive,
            width: 640,
            "has-modal-card": "",
            "trap-focus": "",
            "aria-role": "dialog",
            "aria-modal": ""
          },
          on: {
            "update:active": function($event) {
              _vm.isEditComponentModalActive = $event
            }
          }
        },
        [
          _c("edit-staff-component", {
            attrs: { employee: _vm.activeEmployeeData },
            on: { updatedEmployee: _vm.updateEmployeeFromList }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("h1", { staticClass: "title is-1" }, [_vm._v("Staff")]),
      _vm._v(" "),
      _c(
        "form",
        [
          _vm._v("\n        Search by`\n        "),
          _c(
            "b-field",
            { attrs: { grouped: "" } },
            [
              _c(
                "b-field",
                { attrs: { label: "Full name" } },
                [
                  _c("b-input", {
                    model: {
                      value: _vm.searchData.fullName,
                      callback: function($$v) {
                        _vm.$set(_vm.searchData, "fullName", $$v)
                      },
                      expression: "searchData.fullName"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("h5", { staticClass: "title is-5" }, [_vm._v("Results")]),
      _vm._v(" "),
      _c(
        "b-table",
        {
          attrs: { data: _vm.results, loading: _vm.loading },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(props) {
                return [
                  _c(
                    "b-table-column",
                    { attrs: { field: "avatar", width: "64" } },
                    [_c("img", { attrs: { src: props.row.avatar } })]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      attrs: {
                        field: "id",
                        label: "ID",
                        width: "40",
                        numeric: ""
                      }
                    },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.id) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "name", label: "Name" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.name) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "surname", label: "Surname" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.surname) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "patronymic", label: "Patronymic" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.patronymic) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "actions", label: "Actions" } },
                    [
                      _c(
                        "b-button",
                        {
                          attrs: { type: "is-primary" },
                          on: {
                            click: function($event) {
                              return _vm.openEditModal(props.row)
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      )
                    ],
                    1
                  )
                ]
              }
            }
          ])
        },
        [
          _vm._v(" "),
          _c("template", { slot: "empty" }, [
            _c("section", { staticClass: "section" }, [
              _c(
                "div",
                { staticClass: "content has-text-grey has-text-centered" },
                [
                  _c(
                    "p",
                    [
                      _c("b-icon", {
                        attrs: { icon: "emoticon-sad", size: "is-large" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("p", [_vm._v("Nothing here.")])
                ]
              )
            ])
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Staff/CreateStaffComponent.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/Staff/CreateStaffComponent.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateStaffComponent_vue_vue_type_template_id_139ca69a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true& */ "./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true&");
/* harmony import */ var _CreateStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateStaffComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true& */ "./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CreateStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateStaffComponent_vue_vue_type_template_id_139ca69a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateStaffComponent_vue_vue_type_template_id_139ca69a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "139ca69a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Staff/CreateStaffComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateStaffComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=style&index=0&id=139ca69a&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_style_index_0_id_139ca69a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_template_id_139ca69a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/CreateStaffComponent.vue?vue&type=template&id=139ca69a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_template_id_139ca69a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStaffComponent_vue_vue_type_template_id_139ca69a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Staff/EditStaffComponent.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Staff/EditStaffComponent.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditStaffComponent_vue_vue_type_template_id_2c2acac1_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true& */ "./resources/js/components/Staff/EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true&");
/* harmony import */ var _EditStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditStaffComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/Staff/EditStaffComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true& */ "./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _EditStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditStaffComponent_vue_vue_type_template_id_2c2acac1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditStaffComponent_vue_vue_type_template_id_2c2acac1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2c2acac1",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Staff/EditStaffComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Staff/EditStaffComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Staff/EditStaffComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditStaffComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=style&index=0&id=2c2acac1&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_style_index_0_id_2c2acac1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Staff/EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/Staff/EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_template_id_2c2acac1_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Staff/EditStaffComponent.vue?vue&type=template&id=2c2acac1&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_template_id_2c2acac1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditStaffComponent_vue_vue_type_template_id_2c2acac1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/mixins/ErrorsMixin.js":
/*!********************************************!*\
  !*** ./resources/js/mixins/ErrorsMixin.js ***!
  \********************************************/
/*! exports provided: ErrorsMixin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorsMixin", function() { return ErrorsMixin; });
var ErrorsMixin = {
  data: function data() {
    return {
      errors: {}
    };
  },
  methods: {
    hasError: function hasError(field) {
      if (this.errors.hasOwnProperty(field)) {
        return true;
      }

      return false;
    },
    getError: function getError(field) {
      if (this.errors.hasOwnProperty(field)) {
        return this.errors[field];
      }

      return '';
    }
  }
};

/***/ }),

/***/ "./resources/js/pages/StaffPage.vue":
/*!******************************************!*\
  !*** ./resources/js/pages/StaffPage.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& */ "./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&");
/* harmony import */ var _StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StaffPage.vue?vue&type=script&lang=js& */ "./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d7d9ba54",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/StaffPage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/pages/StaffPage.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./StaffPage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&":
/*!*************************************************************************************!*\
  !*** ./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/http.js":
/*!***************************************!*\
  !*** ./resources/js/services/http.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var baseDomain = 'test-task-manage.loc';
var baseURL = "http://".concat(baseDomain, "/api/v1");
var instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: baseURL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content // Authorization: localStorage.getItem('token_type') + ' ' + localStorage.getItem('token')

  }
}); // apply interceptor on response

instance.interceptors.response.use(function (response) {
  // Do something with response data
  return response;
}, function (error) {
  // Do something with response error
  return Promise.reject(error);
});
/* harmony default export */ __webpack_exports__["default"] = (instance);

/***/ })

}]);
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/ErrorsMixin */ "./resources/js/mixins/ErrorsMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'CreateTaskComponent',
  data: function data() {
    return {
      loading: false,
      employees: [],
      filteredEmployees: [],
      form: {
        title: '',
        description: '',
        status_id: 1,
        employees: []
      }
    };
  },
  mixins: [_mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__["ErrorsMixin"]],
  created: function created() {
    this.getEmployees();
  },
  methods: {
    getEmployees: function getEmployees() {
      var _this = this;

      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].get('/staff').then(function (r) {
        _this.employees = _this.filteredEmployees = r.data.data;
        _this.loading = false;
      });
    },
    createTask: function createTask() {
      var _this2 = this;

      this.loading = true;
      this.errors = [];
      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].post('/task', this.form).then(function (r) {
        _this2.$buefy.notification.open({
          message: 'Task created successfully!',
          type: 'is-success'
        });

        _this2.$emit('createdTask', r.data.data);

        _this2.loading = false;
        _this2.form = {};
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this2.errors = error.response.data.errors;
        }

        _this2.loading = false;
      });
    },
    getFilteredEmployees: function getFilteredEmployees(text) {
      this.filteredEmployees = this.employees.filter(function (option) {
        return option.name.toString().toLowerCase().indexOf(text.toLowerCase()) >= 0;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/EditTaskComponent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/ErrorsMixin */ "./resources/js/mixins/ErrorsMixin.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EditTaskComponent',
  props: ['task'],
  data: function data() {
    return {
      loading: false,
      employees: [],
      filteredEmployees: [],
      form: {
        title: this.task.title,
        description: this.task.description,
        status_id: this.task.status_id,
        employees: this.task.employees
      }
    };
  },
  mixins: [_mixins_ErrorsMixin__WEBPACK_IMPORTED_MODULE_1__["ErrorsMixin"]],
  created: function created() {
    this.getEmployees();
  },
  methods: {
    getEmployees: function getEmployees() {
      var _this = this;

      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].get('/staff').then(function (r) {
        _this.employees = _this.filteredEmployees = r.data.data;
        _this.loading = false;
      });
    },
    createTask: function createTask() {
      var _this2 = this;

      this.loading = true;
      this.errors = [];
      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].patch('/task/' + this.task.id, this.form).then(function (r) {
        _this2.$buefy.notification.open({
          message: 'Task created successfully!',
          type: 'is-success'
        });

        _this2.$emit('updatedTask', r.data.data);

        _this2.loading = false;
      })["catch"](function (error) {
        if (error.response.status === 422) {
          _this2.errors = error.response.data.errors;
        }

        _this2.loading = false;
      });
    },
    getFilteredEmployees: function getFilteredEmployees(text) {
      this.filteredEmployees = this.employees.filter(function (option) {
        return option.name.toString().toLowerCase().indexOf(text.toLowerCase()) >= 0;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/TasksPage.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/TasksPage.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _components_Task_CreateTaskComponent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Task/CreateTaskComponent */ "./resources/js/components/Task/CreateTaskComponent.vue");
/* harmony import */ var _components_Task_EditTaskComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Task/EditTaskComponent */ "./resources/js/components/Task/EditTaskComponent.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'TasksPage',
  components: {
    CreateTaskComponent: _components_Task_CreateTaskComponent__WEBPACK_IMPORTED_MODULE_1__["default"],
    EditTaskComponent: _components_Task_EditTaskComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      loading: false,
      isComponentModalActive: false,
      isEditComponentModalActive: false,
      activeTaskData: {},
      tasks: [],
      searchData: {
        title: null,
        description: null,
        status_id: null
      }
    };
  },
  computed: {
    results: function results() {
      return this.tasks;
    }
  },
  watch: {
    searchData: {
      handler: function handler(val) {
        var data = JSON.parse(JSON.stringify(val));
        this.getTasks(data);
      },
      deep: true
    }
  },
  created: function created() {
    this.getTasks();
  },
  methods: {
    getTasks: function getTasks() {
      var _this = this;

      var filter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.loading = true;

      if (filter) {
        filter = Object.keys(filter).filter(function (key) {
          return filter[key] !== 0;
        }).reduce(function (obj, key) {
          obj[key] = filter[key];
          return obj;
        }, {});
      }

      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].get('/task', {
        params: filter
      }).then(function (r) {
        _this.tasks = r.data.data;
        _this.loading = false;
      });
    },
    addTaskToList: function addTaskToList(task) {
      this.tasks.push(task);
    },
    updateTaskFromList: function updateTaskFromList(task) {
      var foundIndex = this.tasks.findIndex(function (x) {
        return x.id === task.id;
      });
      this.tasks[foundIndex] = task;
    },
    openEditModal: function openEditModal(task) {
      this.activeTaskData = task;
      this.isEditComponentModalActive = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "img[data-v-6546c01d] {\n  height: 124px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "img[data-v-b0ff31e2] {\n  height: 124px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      on: {
        submit: function($event) {
          $event.preventDefault()
          return _vm.createTask($event)
        }
      }
    },
    [
      _c(
        "div",
        { staticClass: "modal-card", staticStyle: { width: "640px" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "section",
            { staticClass: "modal-card-body" },
            [
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Title",
                    type: { "is-danger": _vm.hasError("title") },
                    message: _vm.getError("title")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Task Title",
                      required: ""
                    },
                    model: {
                      value: _vm.form.title,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "title", $$v)
                      },
                      expression: "form.title"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Description",
                    type: { "is-danger": _vm.hasError("description") },
                    message: _vm.getError("description")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "textarea",
                      maxlength: "1000",
                      placeholder: "Task De Surname",
                      required: ""
                    },
                    model: {
                      value: _vm.form.description,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "description", $$v)
                      },
                      expression: "form.description"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Status",
                    type: { "is-danger": _vm.hasError("status_id") },
                    message: _vm.getError("status_id")
                  }
                },
                [
                  _c(
                    "b-select",
                    {
                      attrs: { placeholder: "Select a status" },
                      model: {
                        value: _vm.form.status_id,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "status_id", $$v)
                        },
                        expression: "form.status_id"
                      }
                    },
                    [
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("To Do")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v("Doing")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "3" } }, [_vm._v("Done")])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Assign Employees",
                    type: { "is-danger": _vm.hasError("employees") },
                    message: _vm.getError("employees")
                  }
                },
                [
                  _c("b-taginput", {
                    attrs: {
                      data: _vm.filteredEmployees,
                      autocomplete: "",
                      "allow-new": false,
                      "open-on-focus": true,
                      field: "name",
                      placeholder: "Add a Employee"
                    },
                    on: { typing: _vm.getFilteredEmployees },
                    model: {
                      value: _vm.form.employees,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "employees", $$v)
                      },
                      expression: "form.employees"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "footer",
            { staticClass: "modal-card-foot" },
            [
              _c(
                "b-button",
                {
                  attrs: {
                    type: "is-primary",
                    loading: _vm.loading,
                    "native-type": "submit"
                  }
                },
                [_vm._v("Create")]
              )
            ],
            1
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "modal-card-head" }, [
      _c("p", { staticClass: "modal-card-title" }, [_vm._v("Create Task")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Task/EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      on: {
        submit: function($event) {
          $event.preventDefault()
          return _vm.createTask($event)
        }
      }
    },
    [
      _c(
        "div",
        { staticClass: "modal-card", staticStyle: { width: "640px" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "section",
            { staticClass: "modal-card-body" },
            [
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Title",
                    type: { "is-danger": _vm.hasError("title") },
                    message: _vm.getError("title")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "text",
                      placeholder: "Task Title",
                      required: ""
                    },
                    model: {
                      value: _vm.form.title,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "title", $$v)
                      },
                      expression: "form.title"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Description",
                    type: { "is-danger": _vm.hasError("description") },
                    message: _vm.getError("description")
                  }
                },
                [
                  _c("b-input", {
                    attrs: {
                      type: "textarea",
                      maxlength: "1000",
                      placeholder: "Task De Surname",
                      required: ""
                    },
                    model: {
                      value: _vm.form.description,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "description", $$v)
                      },
                      expression: "form.description"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Status",
                    type: { "is-danger": _vm.hasError("status_id") },
                    message: _vm.getError("status_id")
                  }
                },
                [
                  _c(
                    "b-select",
                    {
                      attrs: { placeholder: "Select a status" },
                      model: {
                        value: _vm.form.status_id,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "status_id", $$v)
                        },
                        expression: "form.status_id"
                      }
                    },
                    [
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("To Do")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v("Doing")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "3" } }, [_vm._v("Done")])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                {
                  attrs: {
                    label: "Assign Employees",
                    type: { "is-danger": _vm.hasError("employees") },
                    message: _vm.getError("employees")
                  }
                },
                [
                  _c("b-taginput", {
                    attrs: {
                      data: _vm.filteredEmployees,
                      autocomplete: "",
                      "allow-new": false,
                      "open-on-focus": true,
                      field: "name",
                      placeholder: "Add a Employee"
                    },
                    on: { typing: _vm.getFilteredEmployees },
                    model: {
                      value: _vm.form.employees,
                      callback: function($$v) {
                        _vm.$set(_vm.form, "employees", $$v)
                      },
                      expression: "form.employees"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "footer",
            { staticClass: "modal-card-foot" },
            [
              _c(
                "b-button",
                {
                  attrs: {
                    type: "is-primary",
                    loading: _vm.loading,
                    "native-type": "submit"
                  }
                },
                [_vm._v("Update")]
              )
            ],
            1
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "modal-card-head" }, [
      _c("p", { staticClass: "modal-card-title" }, [_vm._v("Edit Task")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/TasksPage.vue?vue&type=template&id=953764f8&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/TasksPage.vue?vue&type=template&id=953764f8&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "button",
        {
          staticClass: "button is-primary is-pulled-right",
          on: {
            click: function($event) {
              _vm.isComponentModalActive = true
            }
          }
        },
        [_vm._v("\n        Create Task\n    ")]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            active: _vm.isComponentModalActive,
            width: 640,
            "has-modal-card": "",
            "trap-focus": "",
            "aria-role": "dialog",
            "aria-modal": ""
          },
          on: {
            "update:active": function($event) {
              _vm.isComponentModalActive = $event
            }
          }
        },
        [
          _c("create-task-component", {
            on: { createdTask: _vm.addTaskToList }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            active: _vm.isEditComponentModalActive,
            width: 640,
            "has-modal-card": "",
            "trap-focus": "",
            "aria-role": "dialog",
            "aria-modal": ""
          },
          on: {
            "update:active": function($event) {
              _vm.isEditComponentModalActive = $event
            }
          }
        },
        [
          _c("edit-task-component", {
            attrs: { task: _vm.activeTaskData },
            on: { updatedTask: _vm.updateTaskFromList }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("h1", { staticClass: "title is-1" }, [_vm._v("Tasks")]),
      _vm._v(" "),
      _c(
        "form",
        [
          _vm._v("\n        Search by`\n        "),
          _c(
            "b-field",
            { attrs: { grouped: "" } },
            [
              _c(
                "b-field",
                { attrs: { label: "Title" } },
                [
                  _c("b-input", {
                    model: {
                      value: _vm.searchData.title,
                      callback: function($$v) {
                        _vm.$set(_vm.searchData, "title", $$v)
                      },
                      expression: "searchData.title"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                { attrs: { label: "Description" } },
                [
                  _c("b-input", {
                    model: {
                      value: _vm.searchData.description,
                      callback: function($$v) {
                        _vm.$set(_vm.searchData, "description", $$v)
                      },
                      expression: "searchData.description"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-field",
                { attrs: { label: "Status" } },
                [
                  _c(
                    "b-select",
                    {
                      attrs: { placeholder: "Select a status" },
                      model: {
                        value: _vm.searchData.status_id,
                        callback: function($$v) {
                          _vm.$set(_vm.searchData, "status_id", $$v)
                        },
                        expression: "searchData.status_id"
                      }
                    },
                    [
                      _c("option", { domProps: { value: null } }, [
                        _vm._v("All")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "1" } }, [
                        _vm._v("To Do")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "2" } }, [
                        _vm._v("Doing")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "3" } }, [_vm._v("Done")])
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("h5", { staticClass: "title is-5" }, [_vm._v("Results")]),
      _vm._v(" "),
      _c(
        "b-table",
        {
          attrs: { data: _vm.results, loading: _vm.loading },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(props) {
                return [
                  _c(
                    "b-table-column",
                    {
                      attrs: {
                        field: "id",
                        label: "ID",
                        width: "40",
                        numeric: ""
                      }
                    },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.id) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "title", label: "Title" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.title) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "description", label: "Description" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.description) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "status", label: "Status" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.status) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "employees", label: "Employees" } },
                    [
                      _vm._l(props.row.employees, function(employee) {
                        return [
                          _c("span", { staticClass: "tag is-success" }, [
                            _vm._v(
                              "\n                        " +
                                _vm._s(employee.name) +
                                " " +
                                _vm._s(employee.surname) +
                                "\n                    "
                            )
                          ]),
                          _vm._v("  \n                ")
                        ]
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "created_at", label: "Created At" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.created_at) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "actions", label: "Actions" } },
                    [
                      _c(
                        "b-button",
                        {
                          attrs: { type: "is-primary" },
                          on: {
                            click: function($event) {
                              return _vm.openEditModal(props.row)
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      )
                    ],
                    1
                  )
                ]
              }
            }
          ])
        },
        [
          _vm._v(" "),
          _c("template", { slot: "empty" }, [
            _c("section", { staticClass: "section" }, [
              _c(
                "div",
                { staticClass: "content has-text-grey has-text-centered" },
                [
                  _c(
                    "p",
                    [
                      _c("b-icon", {
                        attrs: { icon: "emoticon-sad", size: "is-large" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("p", [_vm._v("Nothing here.")])
                ]
              )
            ])
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Task/CreateTaskComponent.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/Task/CreateTaskComponent.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateTaskComponent_vue_vue_type_template_id_6546c01d_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true& */ "./resources/js/components/Task/CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true&");
/* harmony import */ var _CreateTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateTaskComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/Task/CreateTaskComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true& */ "./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CreateTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateTaskComponent_vue_vue_type_template_id_6546c01d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateTaskComponent_vue_vue_type_template_id_6546c01d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6546c01d",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Task/CreateTaskComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Task/CreateTaskComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Task/CreateTaskComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateTaskComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=style&index=0&id=6546c01d&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_style_index_0_id_6546c01d_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Task/CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/Task/CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_template_id_6546c01d_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/CreateTaskComponent.vue?vue&type=template&id=6546c01d&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_template_id_6546c01d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateTaskComponent_vue_vue_type_template_id_6546c01d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Task/EditTaskComponent.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/Task/EditTaskComponent.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditTaskComponent_vue_vue_type_template_id_b0ff31e2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true& */ "./resources/js/components/Task/EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true&");
/* harmony import */ var _EditTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditTaskComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/Task/EditTaskComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true& */ "./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _EditTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditTaskComponent_vue_vue_type_template_id_b0ff31e2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditTaskComponent_vue_vue_type_template_id_b0ff31e2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "b0ff31e2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Task/EditTaskComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Task/EditTaskComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/Task/EditTaskComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditTaskComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=style&index=0&id=b0ff31e2&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_style_index_0_id_b0ff31e2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Task/EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/Task/EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_template_id_b0ff31e2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Task/EditTaskComponent.vue?vue&type=template&id=b0ff31e2&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_template_id_b0ff31e2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditTaskComponent_vue_vue_type_template_id_b0ff31e2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/mixins/ErrorsMixin.js":
/*!********************************************!*\
  !*** ./resources/js/mixins/ErrorsMixin.js ***!
  \********************************************/
/*! exports provided: ErrorsMixin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorsMixin", function() { return ErrorsMixin; });
var ErrorsMixin = {
  data: function data() {
    return {
      errors: {}
    };
  },
  methods: {
    hasError: function hasError(field) {
      if (this.errors.hasOwnProperty(field)) {
        return true;
      }

      return false;
    },
    getError: function getError(field) {
      if (this.errors.hasOwnProperty(field)) {
        return this.errors[field];
      }

      return '';
    }
  }
};

/***/ }),

/***/ "./resources/js/pages/TasksPage.vue":
/*!******************************************!*\
  !*** ./resources/js/pages/TasksPage.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TasksPage_vue_vue_type_template_id_953764f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TasksPage.vue?vue&type=template&id=953764f8&scoped=true& */ "./resources/js/pages/TasksPage.vue?vue&type=template&id=953764f8&scoped=true&");
/* harmony import */ var _TasksPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TasksPage.vue?vue&type=script&lang=js& */ "./resources/js/pages/TasksPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TasksPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TasksPage_vue_vue_type_template_id_953764f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TasksPage_vue_vue_type_template_id_953764f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "953764f8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/TasksPage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/TasksPage.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/pages/TasksPage.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TasksPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./TasksPage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/TasksPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TasksPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/TasksPage.vue?vue&type=template&id=953764f8&scoped=true&":
/*!*************************************************************************************!*\
  !*** ./resources/js/pages/TasksPage.vue?vue&type=template&id=953764f8&scoped=true& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TasksPage_vue_vue_type_template_id_953764f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./TasksPage.vue?vue&type=template&id=953764f8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/TasksPage.vue?vue&type=template&id=953764f8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TasksPage_vue_vue_type_template_id_953764f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TasksPage_vue_vue_type_template_id_953764f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/http.js":
/*!***************************************!*\
  !*** ./resources/js/services/http.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var baseDomain = 'test-task-manage.loc';
var baseURL = "http://".concat(baseDomain, "/api/v1");
var instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: baseURL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content // Authorization: localStorage.getItem('token_type') + ' ' + localStorage.getItem('token')

  }
}); // apply interceptor on response

instance.interceptors.response.use(function (response) {
  // Do something with response data
  return response;
}, function (error) {
  // Do something with response error
  return Promise.reject(error);
});
/* harmony default export */ __webpack_exports__["default"] = (instance);

/***/ })

}]);
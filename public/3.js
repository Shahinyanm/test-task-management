(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/StaffPage.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/http */ "./resources/js/services/http.js");
/* harmony import */ var _components_Staff_CreateStaffComponent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Staff/CreateStaffComponent */ "./resources/js/components/Staff/CreateStaffComponent.vue");
/* harmony import */ var _components_Staff_EditStaffComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Staff/EditStaffComponent */ "./resources/js/components/Staff/EditStaffComponent.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'StaffPage',
  components: {
    CreateStaffComponent: _components_Staff_CreateStaffComponent__WEBPACK_IMPORTED_MODULE_1__["default"],
    EditStaffComponent: _components_Staff_EditStaffComponent__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      loading: false,
      isComponentModalActive: false,
      isEditComponentModalActive: false,
      activeEmployeeData: {},
      staff: [],
      searchData: {
        fullName: null,
        task: null
      }
    };
  },
  computed: {
    results: function results() {
      return this.staff;
    }
  },
  watch: {
    searchData: {
      handler: function handler(val) {
        var data = JSON.parse(JSON.stringify(val));
        this.getStaff(data);
      },
      deep: true
    }
  },
  created: function created() {
    this.getStaff();
  },
  methods: {
    getStaff: function getStaff() {
      var _this = this;

      var filter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.loading = true;

      if (filter) {
        filter = Object.keys(filter).filter(function (key) {
          return filter[key] !== 0;
        }).reduce(function (obj, key) {
          obj[key] = filter[key];
          return obj;
        }, {});
      }

      _services_http__WEBPACK_IMPORTED_MODULE_0__["default"].get('/staff', {
        params: filter
      }).then(function (r) {
        _this.staff = r.data.data;
        _this.loading = false;
      });
    },
    addEmployeeToList: function addEmployeeToList(employee) {
      this.staff.push(employee);
    },
    updateEmployeeFromList: function updateEmployeeFromList(employee) {
      var foundIndex = this.staff.findIndex(function (x) {
        return x.id === employee.id;
      });
      this.staff[foundIndex] = employee;
    },
    openEditModal: function openEditModal(employee) {
      this.activeEmployeeData = employee;
      this.isEditComponentModalActive = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "button",
        {
          staticClass: "button is-primary is-pulled-right",
          on: {
            click: function($event) {
              _vm.isComponentModalActive = true
            }
          }
        },
        [_vm._v("\n        Create Employee\n    ")]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            active: _vm.isComponentModalActive,
            width: 640,
            "has-modal-card": "",
            "trap-focus": "",
            "aria-role": "dialog",
            "aria-modal": ""
          },
          on: {
            "update:active": function($event) {
              _vm.isComponentModalActive = $event
            }
          }
        },
        [
          _c("create-staff-component", {
            on: { createdEmployee: _vm.addEmployeeToList }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            active: _vm.isEditComponentModalActive,
            width: 640,
            "has-modal-card": "",
            "trap-focus": "",
            "aria-role": "dialog",
            "aria-modal": ""
          },
          on: {
            "update:active": function($event) {
              _vm.isEditComponentModalActive = $event
            }
          }
        },
        [
          _c("edit-staff-component", {
            attrs: { employee: _vm.activeEmployeeData },
            on: { updatedEmployee: _vm.updateEmployeeFromList }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("h1", { staticClass: "title is-1" }, [_vm._v("Staff")]),
      _vm._v(" "),
      _c(
        "form",
        [
          _vm._v("\n        Search by`\n        "),
          _c(
            "b-field",
            { attrs: { grouped: "" } },
            [
              _c(
                "b-field",
                { attrs: { label: "Full name" } },
                [
                  _c("b-input", {
                    model: {
                      value: _vm.searchData.fullName,
                      callback: function($$v) {
                        _vm.$set(_vm.searchData, "fullName", $$v)
                      },
                      expression: "searchData.fullName"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("h5", { staticClass: "title is-5" }, [_vm._v("Results")]),
      _vm._v(" "),
      _c(
        "b-table",
        {
          attrs: { data: _vm.results, loading: _vm.loading },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(props) {
                return [
                  _c(
                    "b-table-column",
                    { attrs: { field: "avatar", width: "64" } },
                    [_c("img", { attrs: { src: props.row.avatar } })]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    {
                      attrs: {
                        field: "id",
                        label: "ID",
                        width: "40",
                        numeric: ""
                      }
                    },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.id) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "name", label: "Name" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.name) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "surname", label: "Surname" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.surname) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "patronymic", label: "Patronymic" } },
                    [
                      _vm._v(
                        "\n                " +
                          _vm._s(props.row.patronymic) +
                          "\n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-table-column",
                    { attrs: { field: "actions", label: "Actions" } },
                    [
                      _c(
                        "b-button",
                        {
                          attrs: { type: "is-primary" },
                          on: {
                            click: function($event) {
                              return _vm.openEditModal(props.row)
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      )
                    ],
                    1
                  )
                ]
              }
            }
          ])
        },
        [
          _vm._v(" "),
          _c("template", { slot: "empty" }, [
            _c("section", { staticClass: "section" }, [
              _c(
                "div",
                { staticClass: "content has-text-grey has-text-centered" },
                [
                  _c(
                    "p",
                    [
                      _c("b-icon", {
                        attrs: { icon: "emoticon-sad", size: "is-large" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("p", [_vm._v("Nothing here.")])
                ]
              )
            ])
          ])
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/pages/StaffPage.vue":
/*!******************************************!*\
  !*** ./resources/js/pages/StaffPage.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& */ "./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&");
/* harmony import */ var _StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StaffPage.vue?vue&type=script&lang=js& */ "./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d7d9ba54",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/StaffPage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/pages/StaffPage.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./StaffPage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&":
/*!*************************************************************************************!*\
  !*** ./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/StaffPage.vue?vue&type=template&id=d7d9ba54&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StaffPage_vue_vue_type_template_id_d7d9ba54_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
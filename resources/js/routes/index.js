function lazyLoad (view) {
  return () => import(`../pages/${view}`)
}

export default [
  {
    path: '/',
    name: 'home',
    component: lazyLoad('HomePage'),
    props: true,
  },
  {
    path: '/staff',
    name: 'staff',
    component: lazyLoad('StaffPage'),
    props: true,
  },
  {
    path: '/tasks',
    name: 'tasks',
    component: lazyLoad('TasksPage'),
    props: true,
  },
]
